import React, { Component } from 'react';
import delIcon from '../img/delete.png'


class Joke extends Component {

    render() {

        return (
            <article className='joke'>
                <span>{this.props.joke}</span>
                <div>
                <img onClick={()=>this.props.deleteJoke(this.props.id)} className='delIcon' src={delIcon}/>
                </div>
            </article>
        )
    }
}







export default Joke;
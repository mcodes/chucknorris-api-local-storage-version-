import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import Joke from './components/Joke';


class App extends Component {
  state = {
    id: '',
    joke: '',
    jokes: [],
    message: '',
  }

  componentWillMount() {
    //checking if it exists in local storage if so we set it to the state.
    localStorage.getItem('jokes') && this.setState({
      jokes: JSON.parse(localStorage.getItem('jokes'))
    })
  }


  addJoke = () => {

    let { jokes, id, joke } = this.state;
    let index = jokes.findIndex(ele => ele.id === id)
    if (joke === '') {
      return
    } else {
      return index === -1 ? (
        jokes.push({ joke, id }),
        localStorage.setItem('jokes', JSON.stringify(jokes)),
        this.setState({ jokes, joke: '' }))
        : this.setState({ message: 'This item is already in your list' })
    }

  }

  deleteJoke = (id) => {
    let jokes = this.state.jokes;
    let index = jokes.findIndex(ele => ele.id === id)
    jokes.splice(index, 1)
    localStorage.setItem('jokes', JSON.stringify(jokes))
    this.setState({ jokes })

  }

  showJoke = async () => {
    try {
      const res = await axios.get('http://api.icndb.com/jokes/random')
      let { id, joke } = res.data.value;
      this.setState({ id, joke, message: '' })
    } catch (error) {
      console.log(error)
    }
  }

  render() {

    let { message, joke } = this.state;

    return (
      <main>
        <div className='newJoke'>
          <button className='btn' onClick={this.showJoke}>CLICK FOR AN AWESOME CHUCK JOKE</button>

          <h1 className='d-inline'>{joke}</h1>

          {this.state.message !== '' && <h5 className='error'>{message}</h5>}

        </div>

        <div className='savedJokes'>
          <button className='btn' onClick={this.addJoke}>+ CLICK TO ADD TO YOUR FAVOURITES</button>
          <div className='savedJokesScroll'>

            {
              this.state.jokes.map((el, i) => {
                return <Joke key={i} {...el} deleteJoke={this.deleteJoke} />
              })
            }

          </div>
        </div>
      </main>
    );
  }




}

export default App;
